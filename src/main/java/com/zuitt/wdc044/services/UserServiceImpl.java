package com.zuitt.wdc044.services;

import com.zuitt.wdc044.config.JwtToken;
import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService{

    @Autowired //injects code from classes/files that we have imported.
    private UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;
    public void createUser(User user){
        userRepository.save(user);
    }

    public Optional<User> findByUsername(String username) {
        return Optional.ofNullable(userRepository.findByUsername(username));
    }


    //[UPDATE USER]
    public ResponseEntity updateUser(Long id, String stringToken, User user){
        User userToBeUpdated = userRepository.findById(id).get();
        String userAuthor = userToBeUpdated.getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUser.equals(userAuthor)){
            userToBeUpdated.setUsername(user.getUsername());
            userToBeUpdated.setPassword(user.getPassword());
            userRepository.save(userToBeUpdated);
            return new ResponseEntity<>("User updated successfully", HttpStatus.OK);
        }else{
            return new ResponseEntity<>("You are not authorized to edit this user's details.", HttpStatus.UNAUTHORIZED);
        }
    }

    //[DELETE USER]
    public ResponseEntity deleteUser(Long id, String stringToken) {
        User userToBeUpdated = userRepository.findById(id).get();
        String userAuthor = userToBeUpdated.getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUser.equals(userAuthor)) {
            userRepository.deleteById(id);
            return new ResponseEntity<>("User deleted successfully", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You are not authorized to delete this user's account.", HttpStatus.UNAUTHORIZED);
        }
    }

    //[GETTING ALL USERS]
    public Iterable<User> getUsers(){
        return userRepository.findAll();
    }

}
