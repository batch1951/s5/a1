package com.zuitt.wdc044.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="users")
public class User {
    //User model properties:
    //id - Long/Primary key/Auto-increments
    //username - String
    //password - String
    //Getters and setters for username and password
    //Getter for id

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String username;

    @Column
    private String password;

    @OneToMany(mappedBy = "user")
    @JsonIgnore //@JsonIgnore is added to avoid infinite nesting when retrieving post content
    private Set<Post> posts;

    public Set<Post> getPosts(){
        return posts;
    }

    public User(){}

    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    public Long getId(){
        return id;
    }

    public String getUsername(){
        return username;
    }

    public void setUsername(String username){
        this.username = username;
    }

    public String getPassword(){
        return password;
    }

    public void setPassword(String password){
        this.password = password;
    }
}
